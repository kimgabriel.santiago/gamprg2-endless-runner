// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EndlessRunnerGameMode.generated.h"

UCLASS(minimalapi)
class AEndlessRunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEndlessRunnerGameMode();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere)
	TSubclassOf<class ATile> TileRef;

	UPROPERTY(EditAnywhere)
	int32 InitialTiles = 10;

	UPROPERTY(EditAnywhere)
	FVector SpawnPoint;

	UFUNCTION()
	void CreateTile();

	UFUNCTION()
	void DestroyTile(ATile* tile);




};




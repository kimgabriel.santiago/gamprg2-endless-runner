// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"



UCLASS()
class ENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()

public:
	ARunCharacterController();

	
protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class ARunCharacter* RunCharacter;
	virtual void SetupInputComponent() override;
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;

	void MoveForward(float scale);
	void MoveRight(float scale);
	


};

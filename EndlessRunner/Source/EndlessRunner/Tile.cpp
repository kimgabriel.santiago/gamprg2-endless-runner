// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "Math/UnrealMathUtility.h"
#include "Obstacle.h"
#include "Kismet/KismetMathLibrary.h"
#include "Pickup.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	AttachPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("AttachPoint"));
	AttachPoint->SetupAttachment(SceneComponent);

	ExitTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("ExitTrigger"));
	ExitTrigger->SetupAttachment(SceneComponent);
	ExitTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnBeginOverlap);

	ObjectSpawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("ObstacleSpawnBox"));
	ObjectSpawnBox->SetupAttachment(SceneComponent);


}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	
	int32 spawnCount = FMath::RandRange(1, 2);
	int32 randNum = FMath::RandRange(0, 5);
	
	if (randNum < 3)
	{
		for (int i = 0; i < spawnCount; i++)
		{
			SpawnObstacle();
		}
	}
	else
	{
		for (int i = 0; i < spawnCount; i++)
		{
			SpawnPickup();
		}
	}
	
	

}

void ATile::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ARunCharacter* Character = Cast<ARunCharacter>(OtherActor))
	{
		UE_LOG(LogTemp, Warning, TEXT("Hit"));
		OnExited.Broadcast(this);
	}
}


// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::DestroyActor()
{
	if (this != nullptr)
	{
		Destroy();
	}
}

void ATile::SpawnObstacle()
{
	TSubclassOf<class AObstacle> chosenObstacle;

	int32 randIndex = FMath::RandRange(0, ObstacleClass.Num() - 1);
	 
	chosenObstacle = ObstacleClass[randIndex];

	FTransform transform;
	transform.SetLocation(UKismetMathLibrary::RandomPointInBoundingBox(ObjectSpawnBox->GetComponentLocation(), ObjectSpawnBox->GetScaledBoxExtent()));

	AObstacle* obstacle = GetWorld()->SpawnActor<AObstacle>(chosenObstacle, GenerateSpawnLocation(ObjectSpawnBox));

	obstacle->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	spawnedObject.Add(obstacle);
	
}

void ATile::SpawnPickup()
{
	TSubclassOf<class APickup> chosenPickupObj;

	int32 randIndex = FMath::RandRange(0, PickupClass.Num() - 1);

	chosenPickupObj = PickupClass[randIndex];

	FTransform transform;
	transform.SetLocation(UKismetMathLibrary::RandomPointInBoundingBox(ObjectSpawnBox->GetComponentLocation(), ObjectSpawnBox->GetScaledBoxExtent()));

	APickup* pickup = GetWorld()->SpawnActor<APickup>(chosenPickupObj, GenerateSpawnLocation(ObjectSpawnBox));

	pickup->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);

	spawnedObject.Add(pickup);

}

void ATile::Destroyed()
{
	for (AActor* spawned : spawnedObject)
	{
		spawned->Destroy();
	}
}

FTransform ATile::GenerateSpawnLocation(const UBoxComponent* box)
{
	FVector randomPoint = UKismetMathLibrary::RandomPointInBoundingBox(box->GetRelativeLocation(), box->GetScaledBoxExtent());
	return FTransform(box->GetRelativeRotation().Quaternion(), randomPoint, box->GetRelativeScale3D());
}

FVector ATile::GetAttachPointLocation()
{
	return AttachPoint->GetComponentLocation();
}




// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FTileExitedSignature, class ATile*, Tile);

UCLASS()
class ENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(BlueprintAssignable)
	FTileExitedSignature OnExited;

	// Sets default values for this actor's properties
	ATile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* ExitTrigger;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UBoxComponent* ObjectSpawnBox;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<class AObstacle>> ObstacleClass;

	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<class APickup>> PickupClass;


	TArray<class AActor*> spawnedObject;


	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void SpawnObstacle();

	UFUNCTION()
	void SpawnPickup();

	virtual void Destroyed() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void DestroyActor();
	

	int32 ObstacleSpawnRate;
	int32 PickupSpawnRate;

	FTransform GenerateSpawnLocation(const UBoxComponent* box);

	FVector GetAttachPointLocation();

	
	
};

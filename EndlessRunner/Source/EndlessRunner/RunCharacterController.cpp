// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"


ARunCharacterController::ARunCharacterController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();
	RunCharacter = Cast<ARunCharacter>(GetPawn());
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (!RunCharacter->isDead)
	{
		MoveForward(1);
	}

}

void ARunCharacterController::MoveForward(float scale)
{
	RunCharacter->AddMovementInput(RunCharacter->GetActorForwardVector() * scale);
}

void ARunCharacterController::MoveRight(float scale)
{
	RunCharacter->AddMovementInput(RunCharacter->GetActorRightVector() * scale);
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);

}



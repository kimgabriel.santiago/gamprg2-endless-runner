// Copyright Epic Games, Inc. All Rights Reserved.

#include "EndlessRunnerGameMode.h"
#include "EndlessRunnerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Tile.h"
#include "Components/ArrowComponent.h"

AEndlessRunnerGameMode::AEndlessRunnerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AEndlessRunnerGameMode::BeginPlay()
{
	Super::BeginPlay();
	
	for (int i = 0; i < InitialTiles; i++)
	{
		CreateTile();
	}
}

void AEndlessRunnerGameMode::CreateTile()
{
	UE_LOG(LogTemp, Warning, TEXT("Start Spawning"));

	FActorSpawnParameters spawnParameters;

	if (TileRef != nullptr)
	{
		ATile* TileActor = GetWorld()->SpawnActor<ATile>(TileRef, SpawnPoint, FRotator(0), spawnParameters);

		SpawnPoint = TileActor->GetAttachPointLocation();

		TileActor->OnExited.AddDynamic(this, &AEndlessRunnerGameMode::DestroyTile);
	}

}

void AEndlessRunnerGameMode::DestroyTile(ATile* tile)
{
	FTimerHandle Timer;

	FTimerDelegate TimerDelegate;

	TimerDelegate.BindLambda([tile] {
		tile->Destroy();
		});

	GetWorldTimerManager().SetTimer(Timer, TimerDelegate, 2.0f, false);

	CreateTile();

}
